package sqlBuilder

import (
	"fmt"
	"gitlab.com/loxe-tools/go-meta"
)

type columnComparison struct {
	aliasPath  meta.ColumnSet
	operator   string
	namedParam string
}

func IsNull(aliasPath ...meta.Column) *boolExpression {
	if len(aliasPath) == 0 {
		// TODO: throw missing where column
		panic("a")
	}

	return &boolExpression{
		comparison: &columnComparison{aliasPath: aliasPath, operator: "IS", namedParam: "NULL"},
	}
}

func Equal(namedParam string, aliasPath ...meta.Column) *boolExpression {
	if len(aliasPath) == 0 {
		// TODO: throw missing where column
		panic("a")
	}

	return &boolExpression{
		comparison: &columnComparison{aliasPath: aliasPath, operator: "=", namedParam: namedParam},
	}
}

// -----

type boolComparison struct {
	operator string
	with     *boolExpression
}

type boolExpression struct {
	comparison *columnComparison
	boolean    *boolExpression
	compare    *boolComparison
}

func (b *boolExpression) buildBool(with *boolExpression, operator string) *boolExpression {
	if b.compare != nil {
		b.compare.with.buildBool(with, operator)
		return b
	}

	b.compare = &boolComparison{
		operator: operator,
		with:     with,
	}
	return b
}

func (b *boolExpression) And(with *boolExpression) *boolExpression {
	return b.buildBool(with, "AND")
}

func (b *boolExpression) Or(with *boolExpression) *boolExpression {
	return b.buildBool(with, "OR")
}

func Expression(bool *boolExpression) *boolExpression { return &boolExpression{boolean: bool} }

// -----

type where struct {
	selectBuilder *_select
}

func (w *where) Build() string { return w.selectBuilder.sql }

func (w *where) Where(b *boolExpression) *where {
	w.selectBuilder.sql += "WHERE " + w.iterate(b)
	return w
}

func (w *where) iterate(exp *boolExpression) string {
	expression := ""
	if exp.comparison != nil {
		clause := fmt.Sprintf("%s %s", exp.comparison.operator, exp.comparison.namedParam)
		tableName := w.selectBuilder.table.Table()
		selectColumns := w.selectBuilder.columns
		expression = findAliases(clause, tableName, selectColumns, exp.comparison.aliasPath...)
	}
	if exp.boolean != nil {
		expression = fmt.Sprintf("(%s)", w.iterate(exp.boolean))
	}

	if exp.compare == nil {
		return expression
	}

	expression += fmt.Sprintf(" %s ", exp.compare.operator)
	if exp.comparison != nil {
		expression += w.iterate(exp.compare.with)
	} else {
		expression += fmt.Sprintf("(%s)", w.iterate(exp.compare.with))
	}

	return expression
}

func findAliases(clause, path string, columns meta.ColumnSet, aliasPath ...meta.Column) string {
	if len(aliasPath) == 0 {
		// TODO: throw missing where column error
		panic("a")
	}

	columnA := aliasPath[0]
	if len(aliasPath) == 1 || !columnA.IsForeign() {
		return fmt.Sprintf("%s.%s %s", path, columnA.Name(), clause)
	}

	for _, columnB := range columns {
		if !columnB.IsForeign() {
			continue
		}

		newPath := buildAlias(path, columnB)
		if columnA.Cmp(columnB) {
			if len(aliasPath) == 1 {
				return fmt.Sprintf("%s.%s %s", path, columnA.Name(), clause)
			}

			return findAliases(clause, newPath, columnB.Columns(), aliasPath[1:]...)
		}

		if len(aliasPath) > 1 {
			findAliases(clause, newPath, columnB.Columns(), aliasPath...)
		}
	}

	return ""
}
