package sqlBuilder

type joinOrWhere struct {
	selectBuilder *_select
}

func (m *joinOrWhere) Join() *join {
	return &join{selectBuilder: m.selectBuilder}
}

func (m *joinOrWhere) Where(expression *boolExpression) *where {
	w := &where{selectBuilder: m.selectBuilder}
	return w.Where(expression)
}
