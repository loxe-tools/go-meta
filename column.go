package meta

type Column interface {
	Name() string
	FieldName() string
	Cmp(c Column) bool

	// Database relationship abstraction
	IsForeign() bool
	Columns() ColumnSet
	Parent() Entity
	Foreign() Entity
	With(columns ...Column) Column
}

func NewColumn(fieldName, name string, parent Entity) Column {
	return &column{
		fieldName: fieldName,
		name:      name,
		parent:    parent,
	}
}

func NewForeignColumn(fieldName, name string, parent Entity, foreign Entity) Column {
	return &foreignColumn{
		fieldName: fieldName,
		name:      name,
		parent:    parent,
		foreign:   foreign,
		columns:   nil,
	}
}

// -----

type column struct {
	parent    Entity
	name      string
	fieldName string
}

func (m *column) Cmp(otherMeta Column) bool {
	if otherMeta.IsForeign() {
		return false
	}

	sameColumn := m.name == otherMeta.Name()
	sameField := m.fieldName == otherMeta.FieldName()
	sameParentTable := m.parent.Table() == otherMeta.Parent().Table()
	sameParentStruct := m.parent.Struct() == otherMeta.Parent().Struct()
	return sameColumn && sameField && sameParentTable && sameParentStruct
}

func (m *column) IsForeign() bool   { return false }
func (m *column) Name() string      { return m.name }
func (m *column) Parent() Entity    { return m.parent }
func (m *column) FieldName() string { return m.fieldName }

func (m *column) Foreign() Entity               { panic("a") /* TODO: throw error */ }
func (m *column) Columns() ColumnSet            { panic("a") /* TODO: throw error */ }
func (m *column) With(columns ...Column) Column { panic("a") /* TODO: throw error */ }

// -----

type foreignColumn struct {
	fieldName string
	name      string

	parent  Entity
	foreign Entity
	columns []Column
}

func (m *foreignColumn) Cmp(otherMeta Column) bool {
	if !otherMeta.IsForeign() {
		return false
	}

	sameColumn := m.name == otherMeta.Name()
	sameField := m.fieldName == otherMeta.FieldName()
	sameParentTable := m.parent.Table() == otherMeta.Parent().Table()
	sameParentStruct := m.parent.Struct() == otherMeta.Parent().Struct()
	return sameColumn && sameField && sameParentTable && sameParentStruct
}

func (m *foreignColumn) With(columns ...Column) Column {
	if len(columns) == 0 {
		// TODO: fix error
		panic("s")
	}

	for i := 0; i < len(columns); i++ {
		for j := i + 1; j < len(columns); j++ {
			if columns[i].Name() == columns[j].Name() {
				// TODO: Colunas duplicadas nao sao permitidas
				panic("a")
			}
		}
	}
	return &foreignColumn{
		fieldName: m.fieldName,
		name:      m.name,
		parent:    m.parent,
		foreign:   m.foreign,
		columns:   columns,
	}
}

func (m *foreignColumn) IsForeign() bool    { return true }
func (m *foreignColumn) Name() string       { return m.name }
func (m *foreignColumn) Parent() Entity     { return m.parent }
func (m *foreignColumn) Foreign() Entity    { return m.foreign }
func (m *foreignColumn) Columns() ColumnSet { return m.columns }
func (m *foreignColumn) FieldName() string  { return m.fieldName }
