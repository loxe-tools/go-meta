package main

import (
	"bufio"
	"bytes"
	"fmt"
	"os"
	"text/template"
)

type EntityStructFields struct {
	FieldName          string
	FieldNameUncap     string
	FieldNameSnake     string
	FieldForeignStruct string
}

func metafyEntity(filepath string, args *arguments) string {
	f, e := os.Open(fmt.Sprintf("%s/%s", args.directory, filepath))
	if e != nil {
		panic(e)
	}

	reader := bufio.NewReader(f)
	structName, tableNameAlias := extractName(reader)
	fields := extractFieldsEntity(reader)

	structFields := make([]*EntityStructFields, len(fields))
	for i, field := range fields {
		structFields[i] = &EntityStructFields{
			FieldName:          field.fieldName,
			FieldNameUncap:     uncapitalize(field.fieldName),
			FieldNameSnake:     field.fieldNameDB,
			FieldForeignStruct: field.foreignStruct,
		}
	}

	tableName := toSnakeCase(structName) + "s"
	if tableNameAlias != "" {
		tableName = tableNameAlias
	}

	templateVars := EntityTemplateVars{
		PackageName:          args.outputPackage,
		MetaImport:           args.metaImport,
		StructName:           structName,
		TableName:            tableName,
		PkgPrivateStructName: uncapitalize(structName),
		StructFields:         structFields,
	}
	tmpl, e := template.New("").Parse(entityTemplate)
	if e != nil {
		panic(e)
	}

	var data bytes.Buffer
	e = tmpl.Execute(&data, templateVars)
	if e != nil {
		panic(e)
	}

	return data.String()
}
