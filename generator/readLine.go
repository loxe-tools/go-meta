package main

import (
	"bufio"
)

func readLine(reader *bufio.Reader) string {
	lineBuf, _, e := reader.ReadLine()
	if e != nil {
		panic(e)
	}

	return string(lineBuf)
}
