package main

import (
	"bufio"
	"strings"
)

func extractFieldsInput(reader *bufio.Reader) []string {
	fields := make([]string, 0, 10)
	for {
		line := readLine(reader)

		if line == "}" {
			break
		}

		finalIdx := strings.Index(line, " ")
		fields = append(fields, line[1:finalIdx])
	}

	return fields
}
