package main

import (
	"regexp"
	"strings"
)

func uncapitalize(s string) string {
	if s == strings.ToUpper(s) {
		return strings.ToLower(s)
	}

	return strings.ToLower(s[:1]) + s[1:]
}

var matchFirstCap = regexp.MustCompile("(.)([A-Z][a-z]+)")
var matchAllCap = regexp.MustCompile("([a-z0-9])([A-Z])")

func toSnakeCase(str string) string {
	snake := matchFirstCap.ReplaceAllString(str, "${1}_${2}")
	snake = matchAllCap.ReplaceAllString(snake, "${1}_${2}")
	return strings.ToLower(snake)
}
