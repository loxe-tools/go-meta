package main

import (
	"bufio"
	"bytes"
	"fmt"
	"os"
	"text/template"
)

type InputStructFields struct {
	FieldName      string
	FieldNameUncap string
}

func metafyInput(filepath string, args *arguments) string {
	f, e := os.Open(fmt.Sprintf("%s/%s", args.directory, filepath))
	if e != nil {
		panic(e)
	}

	reader := bufio.NewReader(f)
	structName, _ := extractName(reader)
	fields := extractFieldsInput(reader)

	structFields := make([]*InputStructFields, len(fields))
	for i, field := range fields {
		structFields[i] = &InputStructFields{
			FieldName:      field,
			FieldNameUncap: uncapitalize(field),
		}
	}

	templateVars := InputTemplateVars{
		PackageName:          args.outputPackage,
		MetaImport:           args.metaImport,
		StructName:           structName,
		PkgPrivateStructName: uncapitalize(structName),
		StructFields:         structFields,
	}
	tmpl, e := template.New("").Parse(inputTemplate)
	if e != nil {
		panic(e)
	}

	var data bytes.Buffer
	e = tmpl.Execute(&data, templateVars)
	if e != nil {
		panic(e)
	}

	return data.String()
}
