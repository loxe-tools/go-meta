package meta

type ColumnSet []Column

func (set ColumnSet) Pick(columnToPick Column) ColumnSet {
	if !columnToPick.IsForeign() {
		return nil
	}

	for _, column := range set {
		if column.IsForeign() && column.Cmp(columnToPick) {
			return column.Columns()
		}
	}

	return nil
}

func (set ColumnSet) Extract(columnToExtract Column) (Column, ColumnSet) {
	for i, column := range set {
		if column.Cmp(columnToExtract) {
			return column, append(set[:i], set[i+1:]...)
		}
	}

	return nil, set
}

func (set ColumnSet) Merge(newColumns ColumnSet) ColumnSet {
	if len(newColumns) == 0 {
		return set
	}

	newSlice := make(ColumnSet, len(set))
	copy(newSlice, set)

	for _, newColumn := range newColumns {
		contains := false

		for i, column := range set {
			if column.FieldName() == newColumn.FieldName() {
				if column.IsForeign() {
					newSlice[i] = column.With(column.Columns().Merge(newColumn.Columns())...)
				}
				contains = true
				break
			}
		}

		if !contains {
			newSlice = append(newSlice, newColumn)
		}
	}

	return newSlice
}